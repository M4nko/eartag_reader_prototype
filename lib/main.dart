import 'dart:ui';

import 'package:eartag_reader_prototype/routes.dart';
import 'package:eartag_reader_prototype/themes/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'generated/l10n.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  TextStyle defaultStyle() {
    return TextStyle(fontSize: 24);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EarTag Reader Prototype',
      localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: AppColors.main,
          cardTheme: CardTheme(
            elevation: 5,
            margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
          ),
          floatingActionButtonTheme:
              FloatingActionButtonThemeData(backgroundColor: Colors.white),
          iconTheme: IconThemeData(
            size: 28,
            color: Theme.of(context).accentColor,
          ),
          textTheme: TextTheme(
            bodyText1: defaultStyle(),
            bodyText2: defaultStyle(),
          ),
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
          inputDecorationTheme: InputDecorationTheme(
            filled: true,
            isDense: true,
            contentPadding: EdgeInsets.all(4),
          ),
          chipTheme: ChipThemeData(
            backgroundColor: Colors.white,
            disabledColor: Colors.white,
            secondarySelectedColor: AppColors.selected,
            shape: StadiumBorder(),
            selectedColor: AppColors.selected,
            labelStyle: defaultStyle().copyWith(
                fontFamily: 'RobotoMono',
                fontFeatures: [
                  FontFeature.tabularFigures(),
                  FontFeature.slashedZero(),
                ],
                letterSpacing: 0,
                wordSpacing: 0),
            secondaryLabelStyle: defaultStyle(),
            brightness: Brightness.light,
            padding: EdgeInsets.all(0),
          )),
      routes: routes,
      initialRoute: '/',
    );
  }
}
