import 'package:eartag_reader_prototype/components/sheep_card.dart';
import 'package:eartag_reader_prototype/themes/app_icons.dart';
import 'package:flutter/material.dart';

class SheepListHeader extends StatefulWidget {
  final VoidCallback onAddAssisted;
  final VoidCallback onAddBarcode;
  final VoidCallback onAddWithTagReader;
  final VoidCallback onDeselect;
  final VoidCallback onDelete;
  final VoidCallback onEdit;
  final VoidCallback onShowInfo;

  SheepListHeader({
    this.onAddAssisted,
    this.onAddBarcode,
    this.onAddWithTagReader,
    this.onDelete,
    this.onEdit,
    this.onDeselect,
    this.onShowInfo,
  });
  @override
  SheepListHeaderState createState() {
    return SheepListHeaderState();
  }
}

class SheepListHeaderState extends State<SheepListHeader> {
  bool isAdding;
  SheepListHeaderState({this.isAdding = true});

  @override
  Widget build(BuildContext context) {
    final size = Theme.of(context).iconTheme.size;
    List<IconButton> commands;
    if (isAdding) {
      commands = [
        IconButton(
          icon: Icon(
            AppIcons.textRecognition,
            size: size,
          ),
          onPressed: () {
            widget.onAddAssisted();
            Navigator.pushNamed(context, '/pic');
          },
        ),
        IconButton(
          icon: Icon(AppIcons.barCodeRecognition, size: size),
          onPressed: widget.onAddBarcode,
        ),
        IconButton(
          icon: Icon(
            AppIcons.rfNumberDetection,
            size: size,
          ),
          enableFeedback: false,
          onPressed: () {
            widget.onAddWithTagReader();
            setState(() {
              isAdding = false;
            });
          },
        ),
      ];
    } else {
      commands = [
        IconButton(
          icon: Icon(
            AppIcons.close,
            size: size,
          ),
          onPressed: () {
            widget.onDeselect();
            setState(() {
              isAdding = true;
            });
          },
        ),
        IconButton(
          icon: Icon(AppIcons.delete, size: size),
          onPressed: widget.onDelete,
        ),
        IconButton(
          icon: Icon(AppIcons.edit, size: size),
          onPressed: widget.onEdit,
        ),
        IconButton(
          icon: Icon(
            AppIcons.information,
            size: size,
          ),
          enableFeedback: false,
          onPressed: widget.onShowInfo,
        ),
      ];
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(child: Center(child: Text('Schafe'))),
          Material(
            color: Theme.of(context).floatingActionButtonTheme.backgroundColor,
            borderRadius: BorderRadius.all(Radius.circular(32)),
            clipBehavior: Clip.antiAlias,
            elevation: 8,
            child: ButtonBar(
              mainAxisSize: MainAxisSize.min,
              children: commands,
            ),
          )
        ],
      ),
    );
  }
}

class SheepList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SheepListState(_range(0, 10)
        .map((e) => SelectableSheepInfo('CH $e$e$e$e $e$e$e$e', 'Sheep $e'))
        .toList());
  }
}

class SheepListState extends State<SheepList> {
  final List<SelectableSheepInfo> list;

  SheepListState(this.list);
  @override
  Widget build(BuildContext context) {
    final size = Theme.of(context).iconTheme.size;
    return OrientationBuilder(
      builder: (context, orientation) {
        return Expanded(
          child: Card(
            margin: EdgeInsets.zero,
            color: Colors.transparent,
            clipBehavior: Clip.antiAlias,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
            ),
            elevation: 0,
            child: GridView.extent(
              padding: EdgeInsets.symmetric(vertical: 4),
              shrinkWrap: true,
              maxCrossAxisExtent: 500,
              semanticChildCount: list.length,
              childAspectRatio: 3.17 / 1,
              children: list
                  .map((e) => SheepCard(
                        e,
                        true,
                        key: UniqueKey(),
                      ))
                  .toList(),
            ),
          ),
        );
      },
    );
  }
}

Iterable<int> _range(int start, int end) sync* {
  for (int i = start; i < end; i++) yield i;
}
