import 'package:eartag_reader_prototype/themes/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColorList extends StatelessWidget {
  final List<Color> colors;

  ColorList(Color color)
      : this.colors = AppColors.getShadesAndTints(color, 10, 0.11);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: colors.length,
        itemBuilder: (BuildContext context, int i) => Container(
              color: colors[i],
              height: 50,
              child: Text(
                  'i:$i a:${colors[i].alpha} r:${colors[i].red} g:${colors[i].green} b:${colors[i].blue}'),
            ));
  }
}
