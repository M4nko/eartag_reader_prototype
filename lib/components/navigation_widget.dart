import 'package:eartag_reader_prototype/themes/app_colors.dart';
import 'package:eartag_reader_prototype/themes/app_icons.dart';
import 'package:flutter/material.dart';

class NavigationWidget extends StatefulWidget {
  final int pages;
  static const double iconSize = 32;

  const NavigationWidget(this.pages);

  @override
  NavigationState createState() {
    return NavigationState(5);
  }
}

class NavigationState extends State<NavigationWidget> {
  int currentPage = 0;
  List<bool> errorPages;

  get _firstPage => 0;
  get _lastPage => widget.pages > 0 ? widget.pages - 1 : 0;

  NavigationState(int pages) {
    errorPages = List<bool>.filled(pages, false, growable: true);
  }
  void _onLeftFabTaped() {
    if (currentPage != _firstPage) {
      setState(() {
        currentPage--;
      });
    }
  }

  void _onRightFabTaped() {
    if (currentPage != _lastPage) {
      setState(() {
        currentPage++;
      });
    } else if (currentPage == _lastPage) {}
  }

  Widget _middlePartBuilder(BuildContext context) {
    var navpoints = List<Widget>();
    for (int i = 0; i < widget.pages; i++) {
      var color = currentPage == i ? AppColors.selected : AppColors.darkgray;
      double size = currentPage == i ? 20 : 12;
      double margin = currentPage == i ? 0 : 4;
      if (errorPages[i] == true) color = AppColors.error;
      navpoints.add(AnimatedContainer(
        margin: EdgeInsets.symmetric(vertical: margin),
        duration: Duration(milliseconds: 100),
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
        height: size,
        width: size,
      ));
    }
    return Material(
      color: Theme.of(context).floatingActionButtonTheme.backgroundColor,
      elevation: 8,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16))),
      child: Padding(
        padding: const EdgeInsets.all(6.0),
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: 6,
          children: navpoints,
        ),
      ),
    );
  }

  Widget _fabLeftBuilder(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(milliseconds: 100),
      opacity: currentPage != _firstPage ? 1 : 0,
      child: FloatingActionButton(
        backgroundColor: AppColors.main,
        heroTag: const ValueKey('navFabLeft'),
        child: Icon(
          AppIcons.navLeft,
          size: NavigationWidget.iconSize,
        ),
        onPressed: _onLeftFabTaped,
      ),
    );
  }

  Widget _fabRightBuilder(BuildContext context) {
    var hasError = errorPages.reduce((value, element) => element || value);
    Color color = !hasError ? AppColors.success : AppColors.error;
    var icon = !hasError ? AppIcons.correct : AppIcons.incorrect;
    if (currentPage != _lastPage) {
      color = Theme.of(context).floatingActionButtonTheme.backgroundColor;
      color = AppColors.main;
      icon = AppIcons.navRight;
    }
    return FloatingActionButton(
      heroTag: const ValueKey('navFabRight'),
      backgroundColor: color,
      child: Icon(
        icon,
        size: NavigationWidget.iconSize,
      ),
      onPressed: _onRightFabTaped,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, bottom: 32, left: 32, right: 32),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          _fabLeftBuilder(context),
          if (widget.pages != 0) _middlePartBuilder(context),
          _fabRightBuilder(context),
        ],
      ),
    );
  }
}
