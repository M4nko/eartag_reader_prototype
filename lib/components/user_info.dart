import 'package:eartag_reader_prototype/themes/app_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserInfoWidget extends StatelessWidget {
  final EnterpriseInfo enterpriseInfo;
  final UserInfo userInfo;

  UserInfoWidget(this.userInfo, this.enterpriseInfo);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('${userInfo.firstName} ${userInfo.lastName}'),
                  Text(
                    'Agate Nr.:${userInfo.agateNr}',
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
              TextButton.icon(
                  onPressed: null,
                  icon: Icon(AppIcons.logout),
                  label: Text('Logout')),
            ],
          ),
          Column(
            children: [
              Row(
                children: [
                  Text('TVD-Nr: ${enterpriseInfo.tvdNumber}'),
                  if (enterpriseInfo.amountSheeps != null)
                    Text('Sheeps: ${enterpriseInfo.amountSheeps}'),
                  if (enterpriseInfo.amountGoats != null)
                    Text('Goats: ${enterpriseInfo.amountGoats}'),
                  if (enterpriseInfo.amountCows != null)
                    Text('Cows: ${enterpriseInfo.amountCows}'),
                ],
              ),
            ],
          ),
        ],
      ),
    ));
  }
}

class UserInfo {
  int agateNr;
  String firstName;
  String lastName;
  UserInfo(this.agateNr, this.firstName, this.lastName);

  UserInfo.mockup() {
    agateNr = 3468034;
    firstName = 'Max';
    lastName = 'Züchter';
  }
}

class EnterpriseInfo {
  int tvdNumber;
  String plz;
  String place;
  int amountSheeps;
  int amountGoats;
  int amountCows;

  EnterpriseInfo(this.tvdNumber, this.plz, this.place,
      {this.amountSheeps, this.amountGoats, this.amountCows});
  EnterpriseInfo.mockup() {
    tvdNumber = 9999107;
    plz = '8585';
    place = 'Klarsreuti';
    amountSheeps = 186;
  }
}
