import 'package:eartag_reader_prototype/themes/app_colors.dart';
import 'package:eartag_reader_prototype/themes/app_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SheepInfo {
  final String number;
  final String name;
  final int childrens;
  final bool isMale;
  final bool isDead;

  SheepInfo(this.number, this.name,
      {this.childrens = 0, this.isMale = false, this.isDead = false,});
}
class SelectableSheepInfo extends SheepInfo{
  final bool isSelected;
  SelectableSheepInfo(String number, String name, {childrens = 0, isMale = false, isDead = false,this.isSelected = false})
      : super(number, name,childrens: childrens,isMale: isMale,isDead: isDead);
  SelectableSheepInfo withIsSelected(bool isSelected){
    return new SelectableSheepInfo(this.number,this.name,childrens: this.childrens, isMale: this.isMale,isDead: this.isDead,isSelected: isSelected );
  }
}

class SheepCard extends StatefulWidget {
  final SelectableSheepInfo _sheepInfo;
  final bool _isSelectable;
  static const double iconSize = 32;
  SheepCard(this._sheepInfo, this._isSelectable,{ key,}):super(key: key);
  @override
  SheepCardState createState() {
    return SheepCardState();
  }
}

class SheepCardState extends State<SheepCard> {
  bool _isSelected = true;

  @override
  initState() {
    super.initState();
  }

  Color _getColor() {
    if (widget._sheepInfo.isDead)
      return Colors.grey;
    else
      return widget._sheepInfo.isMale
          ? AppColors.maleColor
          : AppColors.femaleColor;
  }

  IconData _getIconData() {
    if (widget._sheepInfo.isDead)
      return AppIcons.cross;
    //return AppIcons.cross;
    else
      return widget._sheepInfo.isMale ? AppIcons.male : AppIcons.female;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      key: this.widget.key,
      elevation: 0,
      color: _getColor(),
      shadowColor: _isSelected ? Theme.of(context).accentColor : null,
      child: InkWell(
        splashColor: Theme.of(context).accentColor.withOpacity(0.5),
        //focusColor: AppColors.error,
        //hoverColor: AppColors.error,
        //highlightColor: AppColors.error,
        onTap: () {
          setState(() {
            _isSelected = !_isSelected && widget._isSelectable;
            var info = this.widget._sheepInfo;
          });
        },
        borderRadius: BorderRadius.circular(8),
        child: AnimatedContainer(
          foregroundDecoration: BoxDecoration(
              border: Border.all(
                  color: _isSelected
                      ? Theme.of(context).accentColor
                      : Colors.white,
                  width: _isSelected ? 4 : 0),
              borderRadius: BorderRadius.all(Radius.circular(8))),
          duration: Duration(milliseconds: 100),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: 8, top: 6, bottom: 8),
                  child: Icon(
                    _getIconData(),
                    size: SheepCard.iconSize - 4,
                    color: Colors.black,
                  )),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(widget._sheepInfo.number),
                      Text(widget._sheepInfo.name),
                      Text('Lämmer: ${widget._sheepInfo.childrens}'),
                    ],
                  ),
                ),
              ),
              ClipRRect(
                  borderRadius:
                      BorderRadius.horizontal(right: Radius.circular(8)),
                  child: Image.asset(
                    'assets/hambledon_hill_sheep.jpg',
                    height: 108,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
