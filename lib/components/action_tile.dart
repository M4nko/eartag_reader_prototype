import 'package:flutter/material.dart';

class ActionTile extends StatelessWidget {
  final String _title;
  final IconData _iconData;
  final Color _backgroundColor;
  final Function _onTap;
  ActionTile(this._title, this._iconData, this._backgroundColor, this._onTap);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: _backgroundColor,
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: _onTap,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 4),
                child: Icon(
                  _iconData,
                ),
              ),
              Text(_title),
            ],
          ),
        ),
      ),
    );
  }
}
