class Role {
  final String name;
  final bool reusable;

  const Role(this.name, this.reusable);
}
