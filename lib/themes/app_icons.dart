import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class AppIcons {
  static final IconData login = setIcon(
      onIOS: CupertinoIcons.square_arrow_right, onAndroid: MdiIcons.login);
  static final IconData logout = setIcon(
      onIOS: CupertinoIcons.square_arrow_left, onAndroid: MdiIcons.logout);

  static final IconData input = setIcon(
      onIOS: CupertinoIcons.arrow_right_to_line,
      onAndroid: MdiIcons.arrowCollapseRight);
  static final IconData output = setIcon(
      onIOS: MdiIcons.arrowExpandRight, onAndroid: MdiIcons.arrowExpandRight);

  static final IconData import =
      setIcon(onIOS: MdiIcons.mapMarkerDown, onAndroid: MdiIcons.mapMarkerDown);
  static final IconData export =
      setIcon(onIOS: MdiIcons.mapMarkerLeft, onAndroid: MdiIcons.mapMarkerLeft);

  static final IconData navLeft = setIcon(
      onIOS: CupertinoIcons.left_chevron, onAndroid: MdiIcons.chevronLeft);
  static final IconData navRight = setIcon(
      onIOS: CupertinoIcons.right_chevron, onAndroid: MdiIcons.chevronRight);

  static final IconData correct =
      setIcon(onIOS: CupertinoIcons.check_mark, onAndroid: MdiIcons.check);
  static final IconData incorrect = setIcon(
      onIOS: CupertinoIcons.exclamationmark, onAndroid: MdiIcons.exclamation);

  static final IconData close =
      setIcon(onIOS: CupertinoIcons.clear, onAndroid: MdiIcons.close);
  static final IconData delete =
      setIcon(onIOS: CupertinoIcons.delete, onAndroid: MdiIcons.delete);
  static final IconData edit = setIcon(onIOS: CupertinoIcons.pencil_ellipsis_rectangle,onAndroid: MdiIcons.squareEditOutline);

  static final IconData search =
      setIcon(onIOS: CupertinoIcons.search, onAndroid: MdiIcons.magnify);
  static final IconData information = setIcon(
      onIOS: CupertinoIcons.info, onAndroid: MdiIcons.informationVariant);
  static final IconData help =
      setIcon(onIOS: CupertinoIcons.question, onAndroid: MdiIcons.help);
  static final IconData calendar = setIcon(onIOS:CupertinoIcons.calendar_today,onAndroid: MdiIcons.calendar);

  static final IconData male =
      setIcon(onIOS: MdiIcons.genderMale, onAndroid: MdiIcons.genderMale);
  static final IconData female =
      setIcon(onIOS: MdiIcons.genderFemale, onAndroid: MdiIcons.genderFemale);
  static final IconData cross =
      setIcon(onIOS: MdiIcons.christianity, onAndroid: MdiIcons.christianity);

  static final IconData textRecognition = setIcon(
      onIOS: CupertinoIcons.doc_text_viewfinder,
      onAndroid: MdiIcons.textRecognition);
  static final IconData barCodeRecognition = setIcon(
      onIOS: CupertinoIcons.barcode_viewfinder,
      onAndroid: MdiIcons.barcodeScan);
  static final IconData rfNumberDetection = setIcon(
      onIOS: CupertinoIcons.badge_plus_radiowaves_right,
      onAndroid: MdiIcons.radioHandheld);

  static IconData setIcon(
      {IconData onIOS, IconData onAndroid, IconData onUnknonw}) {
    switch (Platform.operatingSystem) {
      case 'ios':
        return onIOS;
      case 'android':
        return onAndroid;
      case 'linux':
      case 'macos':
      case 'windows':
      case 'fuchsia':
      default:
        if (onUnknonw == null)
          return onAndroid;
        else
          return onUnknonw;
    }
  }
}
