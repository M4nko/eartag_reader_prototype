import 'package:flutter/material.dart';

class AppColors {
  static final MaterialColor femaleColor = createColor(0xFFFEB1C8);
  static final MaterialColor maleColor = createColor(0xFF00DCFD);
  static final MaterialColor selected = createColor(0xFF0E7AFB);
  static final MaterialColor main = createColor(0xFF3396FF);
  static final MaterialColor error = createColor(0xFFFF4633);
  static final MaterialColor warning = createColor(0xFFFF7F1F);
  static final MaterialColor success = createColor(0xFF33FF78);
  static final MaterialColor darkgray = createColor(0xFF969696);
  static final MaterialColor lightgGray = createColor(0xFFCFCFCF);

  static MaterialColor createColor(int color) {
    Color original = Color(color);
    var swatch = Map<int, Color>();
    var colors = getShadesAndTints(original, 10, 0.11);
    for (int i = 0; i < colors.length; i++) {
      swatch.putIfAbsent(i == 0 ? 50 : i * 100, () => colors[i]);
    }
    return MaterialColor(color, swatch);
  }

  static List<Color> getShadesAndTints(Color c, int amount, double step) {
    List<Color> colors = List<Color>();
    var half = amount ~/ 2;
    for (int i = -half; i < half; i++) {
      colors.add(fade(c, step * i));
    }
    if (half * 2 < amount) colors.add(fade(c, step * half));
    return colors;
  }

  static Color fade(Color color, double fading) {
    assert(fading >= -1 && fading <= 1);
    if (fading < 0) {
      var shade = 1 + fading;
      return Color.fromARGB(
          255,
          shadeComponent(color.red, shade),
          shadeComponent(color.green, shade),
          shadeComponent(color.blue, shade));
    } else if (fading > 0) {
      var tint = 1 - fading;
      return Color.fromARGB(255, tintComponent(color.red, tint),
          tintComponent(color.green, tint), tintComponent(color.blue, tint));
    } else
      return color;
  }

  static int shadeComponent(int component, double shade) =>
      (component * shade).round();
  static int tintComponent(int component, double tint) =>
      (255 - (255 - component) * tint).round();
}
