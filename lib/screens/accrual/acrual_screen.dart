import 'package:eartag_reader_prototype/components/my_input_date_picker.dart';
import 'package:eartag_reader_prototype/components/navigation_widget.dart';
import 'package:eartag_reader_prototype/components/sheep_list_widgets.dart';
import 'package:eartag_reader_prototype/themes/app_icons.dart';
import 'package:flutter/material.dart';

class AccrualScreen extends StatefulWidget {
  @override
  AccrualScreenState createState() {
    return AccrualScreenState();
  }
}

class AccrualScreenState extends State<AccrualScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Zugang'),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(AppIcons.help),
          )
        ],
      ),
      body: Stack(
        children: [
          Form(
            child: OrientationBuilder(
              builder: (context, orientation) {
                final size = Theme.of(context).iconTheme.size;
                final content = Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        child: MyInputDatePickerFormField(
                          textStyle: Theme.of(context).textTheme.bodyText2,
                          icon: Icon(
                            AppIcons.calendar,
                            size: size,
                          ),
                          initialDate: DateTime.now(),
                          firstDate:
                              DateTime.now().subtract(Duration(days: 40)),
                          lastDate: DateTime.now().add(Duration(days: 7)),
                          onDateSaved: (date) {},
                          fieldHintText: "Datum wann Zugang angekommen ist",
                          fieldLabelText: "Datum",
                        ),
                        onTap: () async {
                          print('taped on InputDatePicker');
                          var date = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate:
                                  DateTime.now().subtract(Duration(days: 40)),
                              lastDate: DateTime.now().add(Duration(days: 7)));
                          print('selected $date');
                        },
                        behavior: HitTestBehavior.translucent,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: TextFormField(
                              decoration: InputDecoration(
                                icon: Icon(
                                  AppIcons.output,
                                  size: size,
                                ),
                                labelText: 'Herkunfts TVD-Nr.',
                              ),
                              style: Theme.of(context).textTheme.bodyText2,
                              maxLength: 7,
                              keyboardType: TextInputType.number,
                              textCapitalization: TextCapitalization.characters,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: FloatingActionButton(
                              clipBehavior: Clip.antiAlias,
                              elevation: 8,
                              child: Icon(
                                AppIcons.search,
                                color: Theme.of(context).accentColor,
                              ),
                              onPressed: () {},
                            ),
                          )
                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      ),
                    ),
                    if (orientation == Orientation.portrait) SheepListHeader(),
                  ],
                );
                if (orientation == Orientation.portrait)
                  return Column(
                    children: [
                      content,
                      if (orientation == Orientation.portrait)
                        Expanded(child: SheepList())
                    ],
                  );
                else
                  return Row(
                    children: [
                      SingleChildScrollView(child: Expanded(child: content)),
                      SheepListHeader(),
                      Expanded(child: SheepList()),
                    ],
                  );
              },
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: NavigationWidget(0),
          ),
        ],
      ),
    );
  }
}
