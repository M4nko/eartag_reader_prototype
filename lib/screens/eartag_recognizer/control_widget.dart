import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

typedef onPressed = void Function();

class ControlWidget extends StatelessWidget {
  final onPressed onSave;
  final onPressed onToggleLight;
  final bool hashFlashlight;

  const ControlWidget(
      {this.onSave, this.onToggleLight, this.hashFlashlight = true});
  @override
  Widget build(BuildContext context) {
    final size = Theme.of(context).iconTheme.size;
    return Material(
      elevation: 8,
      color: Theme.of(context).floatingActionButtonTheme.backgroundColor,
      borderRadius: BorderRadius.all(Radius.circular(32)),
      clipBehavior: Clip.antiAlias,
      child: ButtonBar(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          if (hashFlashlight)
            IconButton(
              icon: Icon(
                MdiIcons.flashlight,
                size: size,
              ),
              onPressed: onToggleLight,
            ),
          IconButton(
            icon: Icon(
              MdiIcons.check,
              size: size,
            ),
            onPressed: onSave,
          )
        ],
      ),
    );
  }
}
