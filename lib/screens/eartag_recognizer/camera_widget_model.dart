import 'dart:collection';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CameraModel with ChangeNotifier {
  //Examples: Dd53453456, CH 2345 6547, ad 2356\t2356, 2352 2345
  static final Pattern _numberPattern =
      RegExp(r'(([A-Z]{2}|[0-9]{3,4})?[0-9]{8})');
  //Matches the first two Letters of a tvd-number
  static final Pattern _countryPrefix = RegExp("[A-Z]{2}");
  static final Pattern _num = RegExp('[0-9]{11}');
  static final Pattern _longNum = RegExp('[0-9]{12}');

  HashMap<String, int> proposals = HashMap<String, int>();
  bool hasFlashlight;
  bool isFlashlightEnabled = false;
  void addEntry(String entry) {
    entry = entry
        .replaceAll('\n', '')
        .replaceAll('\t', '')
        .replaceAll('\r', '')
        .replaceAll('\s', '')
        .toUpperCase();
    _numberPattern.allMatches(entry).forEach((element) {
      String result = element[0];
      if (result.startsWith(_countryPrefix)) {
        result =
            '${result.substring(0, 2)} ${result.substring(2, 6)} ${result.substring(6, 10)}';
      } else if (result.startsWith(_longNum)) {
        result =
            '${result.substring(0, 4)} ${result.substring(4, 8)} ${result.substring(8, 12)}';
      } else if (result.startsWith(_num)) {
        result =
            '${result.substring(0, 3)} ${result.substring(3, 7)} ${result.substring(7, 11)}';
      } else {
        result = 'CH ${result.substring(0, 4)} ${result.substring(4, 8)}';
      }
      log('Detected Eartag Number: $result');
      proposals.update(result, (value) => ++value, ifAbsent: () => 1);
      notifyListeners();
    });
  }

  void clearProposals() {
    proposals.clear();
    notifyListeners();
  }

  void toggleFlashlight() {
    isFlashlightEnabled = !isFlashlightEnabled;
    notifyListeners();
  }
}
