class EarTag {
  String number;
  int recognizeCounter;
  bool sheepInDatabase;

  EarTag(this.number,
      {this.recognizeCounter = -1, this.sheepInDatabase = false});
}

class EntryIndex {
  final int roleIndex;
  final int entryIndex;
  EntryIndex(this.roleIndex, {this.entryIndex = -1}) {
    assert(roleIndex >= 0, 'roleindex points to an entry in a list.');
    assert(entryIndex >= -1, 'entryIndex points to an entry in a list.');
  }

  EntryIndex withEntryIndex(int entryIndex) {
    assert(entryIndex >= 0, 'entryIndex points to an entry in a list.');
    return EntryIndex(roleIndex, entryIndex: entryIndex);
  }

  bool hasEntryIndex() => entryIndex != -1;
}
