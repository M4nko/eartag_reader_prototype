import 'dart:ui';

import 'package:eartag_reader_prototype/models/role.dart';
import 'package:eartag_reader_prototype/screens/eartag_recognizer/camera_widget.dart';
import 'package:eartag_reader_prototype/screens/eartag_recognizer/camera_widget_model.dart';
import 'package:eartag_reader_prototype/screens/eartag_recognizer/control_widget.dart';
import 'package:eartag_reader_prototype/screens/eartag_recognizer/entry_modifier_model.dart';
import 'package:eartag_reader_prototype/screens/eartag_recognizer/entry_modifier_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EarTagRecognizerScreen extends StatefulWidget {
  final List<Role> roles;
  const EarTagRecognizerScreen({this.roles = const [Role('Sheep', true)]});

  @override
  EarTagRecognizerScreenState createState() {
    return EarTagRecognizerScreenState();
  }
}

class EarTagRecognizerScreenState extends State<EarTagRecognizerScreen> {
  Widget _buildProposalList(BuildContext context,
      {Axis orientation = Axis.horizontal, double paddingRight = 0}) {
    return Consumer<CameraModel>(
      builder: (context, cameraModel, child) {
        var entries = cameraModel.proposals.entries.toList()
          ..sort((a, b) => -a.value.compareTo(b.value));
        return ListView.builder(
            padding: EdgeInsets.only(right: paddingRight),
            physics: BouncingScrollPhysics(),
            semanticChildCount: cameraModel.proposals.length,
            //reverse: orientation == Axis.vertical ? true : false,
            shrinkWrap: true,
            scrollDirection: orientation,
            itemCount: cameraModel.proposals.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.only(
                    top: 8, right: 8, bottom: 8, left: (index == 0 ? 12 : 0)),
                child: Consumer<EntryModifierModel>(
                  builder: (context, entryModiferModel, child) {
                    var style = Theme.of(context).textTheme.bodyText2.copyWith(
                        fontFamily: 'RobotoMono',
                        fontFeatures: [
                          FontFeature.tabularFigures(),
                          FontFeature.slashedZero(),
                        ],
                        letterSpacing: 0,
                        wordSpacing: 0);
                    var text = <TextSpan>[];
                    entries[index].key.split(' ').forEach((element) {
                      text.add(TextSpan(text: element, style: style));
                      text.add(TextSpan(text: ' '));
                    });
                    text.removeLast();
                    return ChoiceChip(
                      avatar: CircleAvatar(
                        backgroundColor: entries[index].value > 5
                            ? Colors.green
                            : Colors.orange,
                        child: Text(
                          "${entries[index].value}",
                          style: style,
                        ),
                      ),
                      elevation: 4,
                      pressElevation: 4,
                      selected: entryModiferModel.textController.text ==
                          entries[index].key,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 6, vertical: 8),
                      onSelected: (bool selected) {
                        if (selected) {
                          Provider.of<EntryModifierModel>(context,
                                  listen: false)
                              .setText(entries[index].key);
                        }
                      },
                      //'${proposals[index].recognizeCounter < 10 ? ' ': ''}${proposals[index].number}',
                      label: Text.rich(
                        TextSpan(children: text),
                        maxLines: 1,
                        overflow: TextOverflow.clip,
                      ),
                    );
                  },
                ),
              );
            });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Eartag Recognizer'),
      ),
      body: ChangeNotifierProvider<CameraModel>(
        create: (context) => CameraModel(),
        child: Stack(children: [
          Camera(true),
          SafeArea(
            bottom: true,
            child: ChangeNotifierProvider<EntryModifierModel>(
              create: (context) => EntryModifierModel(widget.roles),
              builder: (context, _) => LayoutBuilder(
                builder: (context, constraints) {
                  var queryData = MediaQuery.of(context);
                  if (queryData.orientation == Orientation.portrait) {
                    return Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            clipBehavior: Clip.none,
                            child: Stack(
                              overflow: Overflow.visible,
                              fit: StackFit.passthrough,
                              alignment: Alignment.centerLeft,
                              children: <Widget>[
                                Container(
                                    height: 64 + 16.0,
                                    alignment: Alignment.topCenter,
                                    child: _buildProposalList(context,
                                        paddingRight: 125 + 8.0)),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 12),
                                    child: ControlWidget(
                                      onToggleLight: () {
                                        Provider.of<CameraModel>(context,
                                                listen: false)
                                            .toggleFlashlight();
                                      },
                                      onSave: () {
                                        Provider.of<EntryModifierModel>(context,
                                                listen: false)
                                            .saveText();
                                        Provider.of<CameraModel>(context,
                                                listen: false)
                                            .clearProposals();
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          EntryModifier(),
                        ],
                      ),
                    );
                  } else {
                    return Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Expanded(child: EntryModifier()),
                          Padding(
                            padding:
                                const EdgeInsets.only(bottom: 12, right: 12),
                            child: ControlWidget(
                              onSave: () {
                                Provider.of<EntryModifierModel>(context,
                                        listen: false)
                                    .saveText();
                                Provider.of<CameraModel>(context, listen: false)
                                    .clearProposals();
                              },
                            ),
                          ),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.only(bottom: 2, right: 4),
                            child: _buildProposalList(context,
                                orientation: Axis.vertical),
                          )),
                        ],
                      ),
                    );
                  }
                },
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
