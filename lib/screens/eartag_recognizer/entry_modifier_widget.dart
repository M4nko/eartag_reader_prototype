import 'package:eartag_reader_prototype/models/role.dart';
import 'package:eartag_reader_prototype/screens/eartag_recognizer/entry_modifier_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

import 'eartag_model.dart';

class EntryModifier extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 12, left: 12, bottom: 12),
      child: Material(
        elevation: 8,
        borderRadius: BorderRadius.all(Radius.circular(32)),
        color: Theme.of(context).floatingActionButtonTheme.backgroundColor,
        clipBehavior: Clip.antiAlias,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.baseline,
          textBaseline: TextBaseline.alphabetic,
          children: <Widget>[
            Consumer<EntryModifierModel>(
              builder: (context, model, child) {
                return PopupMenuButton(
                  padding: EdgeInsets.all(0),
                  itemBuilder: (context) {
                    List<PopupMenuEntry> list = [];
                    var reusables = <Role>[];
                    var reusablesId = <EntryIndex>[];
                    for (var i = 0; i < model.roles.length; i++) {
                      var role = model.roles[i];
                      if (role.reusable) {
                        reusables.add(role);
                        reusablesId.add(EntryIndex(i));
                      } else {
                        EarTag tag = model.entries[i][0];
                        var id =
                            EntryIndex(i, entryIndex: tag == null ? -1 : 0);
                        list.add(PopupMenuItem(
                          value: id,
                          child: Text(model.stringFromEntry(id)),
                        ));
                      }
                    }
                    for (int i = 0; i < reusables.length; i++) {
                      //var role = reusables[i];
                      var id = reusablesId[i];
                      list.add(PopupMenuItem(
                          value: id, child: Text(model.stringFromEntry(id))));

                      var roleEntries = model.entries[id.roleIndex];
                      for (int pos = 0; pos < roleEntries.length; pos++) {
                        var entryId = id.withEntryIndex(pos);
                        list.add(PopupMenuItem(
                            value: entryId,
                            child: Text(model.stringFromEntry(entryId))));
                      }
                    }
                    return list;
                  },
                  initialValue: model.selectedEntry,
                  onSelected: (value) {
                    model.selectedEntry = value;
                    var text = model.selectedEntry.hasEntryIndex()
                        ? model
                            .entries[model.selectedEntry.roleIndex]
                                [model.selectedEntry.entryIndex]
                            .number
                        : '';
                    model.setText(text);
                  },
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 12, top: 13, bottom: 17),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      textBaseline: TextBaseline.alphabetic,
                      children: <Widget>[
                        Text(model.stringFromEntry(model.selectedEntry)),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8),
                          child: Icon(
                            MdiIcons.chevronDown,
                            size: Theme.of(context).iconTheme.size,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
            Expanded(
              child: TextField(
                  controller:
                      Provider.of<EntryModifierModel>(context, listen: false)
                          .textController,
                  decoration: InputDecoration(
                      counterText: '',
                      contentPadding: EdgeInsets.only(
                        top: 16,
                        bottom: 16,
                        left: 16,
                      ),
                      hintText: 'Number',
                      border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.transparent, width: 1),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(30),
                              topRight: Radius.circular(30)))),
                  textCapitalization: TextCapitalization.characters,
                  keyboardType: TextInputType.visiblePassword,
                  maxLines: 1,
                  maxLength: 13,
                  style: Theme.of(context).textTheme.bodyText2),
            )
          ],
        ),
      ),
    );
  }
}
