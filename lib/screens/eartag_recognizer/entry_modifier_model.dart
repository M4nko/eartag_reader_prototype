import 'package:eartag_reader_prototype/models/role.dart';
import 'package:eartag_reader_prototype/screens/eartag_recognizer/eartag_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class EntryModifierModel with ChangeNotifier {
  final List<Role> roles;
  List<List<EarTag>> entries;
  EntryIndex selectedEntry;
  var textController = TextEditingController();

  EntryModifierModel(this.roles) {
    entries = List<List<EarTag>>.from(roles.map((e) {
      if (e.reusable) return List<EarTag>.of([], growable: true);
      return List<EarTag>(1);
    }), growable: false);
    selectedEntry = EntryIndex(0);
  }
  void setText(String text) {
    textController.clearComposing();
    textController.value = TextEditingValue(
        text: text,
        selection: TextSelection.collapsed(offset: textController.text.length),
    );
    notifyListeners();
  }

  void saveText() {
    if (textController.text.isEmpty) return;
    var role = roles[selectedEntry.roleIndex];
    var list = entries[selectedEntry.roleIndex];
    if (!selectedEntry.hasEntryIndex()) {
      if (role.reusable) {
        list.add(EarTag(textController.text));
      } else {
        list[0] = EarTag(textController.text);
      }
      selectedEntry = selectedEntry.withEntryIndex(list.length - 1);
    } else {
      list[selectedEntry.entryIndex] = (EarTag(textController.text));
      //selectedEntry = selectedEntry.withEntryIndex(0);
    }
    notifyListeners();
  }

  void selectEntry(EntryIndex selected) {
    selectedEntry = selected;
    var text = selectedEntry.hasEntryIndex()
        ? entries[selectedEntry.roleIndex][selectedEntry.entryIndex].number
        : '';
    setText(text);
    notifyListeners();
  }

  String stringFromEntry(EntryIndex index) {
    var role = roles[index.roleIndex];
    if (index.hasEntryIndex()) {
      return '${role.reusable ? '${index.entryIndex + 1}. ' : ''}${role.name}';
    } else {
      return 'add ${role.name}';
    }
  }
}
