import 'dart:async';
import 'dart:developer' as dev;
import 'dart:developer';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:eartag_reader_prototype/screens/eartag_recognizer/camera_widget_model.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:torch_compat/torch_compat.dart';

class Camera extends StatefulWidget {
  final bool isFlashlightEnabled;

  Camera(this.isFlashlightEnabled);

  @override
  CameraState createState() {
    return CameraState();
  }
}

enum Commands {
  resume,
  stop,
  pause,
}

class CameraState extends State<Camera> with WidgetsBindingObserver {
  Future<CameraController> _controller;
  Commands _listen = Commands.resume;

  //bool _isDetecting;
  Commands get listen => _listen;

  void listenAgain() async {
    var tempDir = await getTemporaryDirectory();
    var file = File('${tempDir.path}/temp.jpg)}');
    var recognizer = FirebaseVision.instance.textRecognizer();

    if (await file.exists()) {
      await file.delete();
    }

    void body() async {
      switch (listen) {
        case Commands.resume:
          _analyseFile(await _controller, recognizer, file).then((result) {
            if (result != null && context != null) {
              Provider.of<CameraModel>(context, listen: false)
                  .addEntry(result.text);
            }
            body();
          });
          break;
        case Commands.stop:
          await recognizer.close();
          if (await file.exists()) {
            await file.delete();
          }
          break;
        case Commands.pause:
          Future.delayed(Duration(milliseconds: 500)).then((value) => {body()});
          break;
      }
    }

    body();
  }

  static Future<VisionText> _analyseFile(CameraController controller,
      TextRecognizer recognizer, File imageFile) async {
    if (controller == null) return null;
    try {
      if (!controller.value.isInitialized) return null;
      await controller.takePicture(imageFile.path);
      var result = await recognizer
          .processImage(FirebaseVisionImage.fromFile(imageFile));
      if (await imageFile.exists()) {
        await imageFile.delete();
      }
      if (result.text.isNotEmpty) {
        return result;
      }
    } catch (e) {
      dev.log('Error dectecting text', error: e);
    }
    return null;
  }

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _controller = _initCamera();
    TorchCompat.hasTorch.then((value) {
      var model = Provider.of<CameraModel>(context, listen: false);
      model.hasFlashlight = value;
      model.addListener(toggleTorch);
    });
  }

  void toggleTorch() {
    var model = Provider.of<CameraModel>(context, listen: false);
    if (model.hasFlashlight) {
      log("toggle flashlight to ${model.isFlashlightEnabled}");
      if (model.isFlashlightEnabled) {
        TorchCompat.turnOn();
      } else {
        TorchCompat.turnOff();
      }
    }
  }

  Future<CameraController> _initCamera() async {
    var cams = await availableCameras();
    var cameras =
        cams.where((cam) => cam.lensDirection == CameraLensDirection.back);
    if (cameras.isEmpty) {
      Navigator.pop(context);
      _listen = Commands.stop;
      return null;
    }
    dev.log("Init CameraController with ${cameras.first.name}");
    var controller = CameraController(cameras.first, ResolutionPreset.medium,
        enableAudio: false);
    await controller.initialize();
    listenAgain();
    if (mounted) {
      setState(() {});
    }
    return controller;
  }

  void _disposeCamera() async {
    TorchCompat.dispose();
    var controller = await _controller;
    if (controller == null) {
      return;
    }
    if (controller.value.isStreamingImages) await controller.stopImageStream();
    await controller.dispose();
  }

  //https://github.com/flutter/plugins/blob/master/packages/camera/example/lib/main.dart
  @override
  dispose() {
    _listen = Commands.stop;
    WidgetsBinding.instance.removeObserver(this);
    _disposeCamera();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    // App state changed before we got the chance to initialize.
    if (_controller == null) {
      return;
    }
    var controller = await _controller;
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    switch (state) {
      case AppLifecycleState.resumed:
        _controller = _initCamera();
        listenAgain();
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        _listen = Commands.stop;
        _disposeCamera();
        break;
      case AppLifecycleState.detached:
        _listen = Commands.stop;
        _disposeCamera();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<CameraController>(
        future: _controller,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(child: Text(snapshot.error.toString()));
            }
            return GestureDetector(
              onTap: () async {
                switch (_listen) {
                  case Commands.resume:
                    _listen = Commands.pause;
                    break;
                  case Commands.stop:
                    break;
                  case Commands.pause:
                    _listen = Commands.resume;
                    break;
                }
              },
              child: LayoutBuilder(
                builder: (context, constraints) => SizedBox.expand(
                  child: FittedBox(
                    fit: BoxFit.cover,
                    child: SizedBox(
                      width: snapshot.data.value.aspectRatio,
                      height: 1,
                      child: CameraPreview(snapshot.data),
                    ),
                  ),
                ),
              ),
            );
          }
          return Center(child: CircularProgressIndicator());
        });
  }

  //This Version tries to analyse the Image directly from the Camera stream.
  //But for some reason the TextRecognizer doesn't recognize anything.
  //My guess is, that there's something wrong with ImageConversion, or with the Image conversion.
  /*
  Future<VisionText> analyseStream(
      CameraImage image, TextRecognizer recognizer) async {
    final WriteBuffer allBytes = WriteBuffer();
    for (Plane plane in image.planes) {
      allBytes.putUint8List(plane.bytes);
    }
    var bytes = allBytes.done().buffer.asUint8List();
    var img = FirebaseVisionImage.fromBytes(
        bytes,
        FirebaseVisionImageMetadata(
            size: Size(image.width.toDouble(), image.height.toDouble()),
            planeData: image.planes
                .map((e) => FirebaseVisionImagePlaneMetadata(
                    bytesPerRow: e.bytesPerRow,
                    height: e.height,
                    width: e.width))
                .toList(),
            rawFormat: image.format.raw,
            rotation: ImageRotation.rotation0));
    var result = recognizer.processImage(img);
    return result;
  }

  Future<void> toggleAnalyseStream(CameraController controller) async {
    if (_listen == Commands.resume && !controller.value.isStreamingImages) {
      var recognizer = FirebaseVision.instance.textRecognizer();
      await controller.startImageStream((image) async {
        if (_isDetecting)
          return;
        else {
          _isDetecting = true;
          var result = await analyseStream(image, recognizer);
          _isDetecting = false;
          dev.inspect(result);
        }
      });
      _listen = Commands.pause;
    }
    if (_listen == Commands.pause && controller.value.isStreamingImages) {
      await controller.stopImageStream();
      _listen = Commands.resume;
    }
  }
   */
}
