import 'package:eartag_reader_prototype/components/action_tile.dart';
import 'package:eartag_reader_prototype/components/sheep_card.dart';
import 'package:eartag_reader_prototype/components/user_info.dart';
import 'package:eartag_reader_prototype/themes/app_colors.dart';
import 'package:eartag_reader_prototype/themes/app_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHomePageScreen extends StatefulWidget {
  MyHomePageScreen({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageScreenState createState() => _MyHomePageScreenState();
}

class _MyHomePageScreenState extends State<MyHomePageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Test'),
      ),
      body: Stack(children: [
        SafeArea(
          child: Center(
            child: Column(
              children: <Widget>[
                //Colorlist(AppColors.main),
                UserInfoWidget(UserInfo.mockup(), EnterpriseInfo.mockup()),
                SheepCard(
                    SelectableSheepInfo('CH 5435 34534', 'Mirta',
                        childrens: 6, isDead: false, isMale: true),
                    true),
                ActionTile('Zugang', AppIcons.input, AppColors.success,
                    () => Navigator.pushNamed(context, '/accrual')),
                ActionTile(
                    'Ohrmarkenleser',
                    AppIcons.textRecognition,
                    AppColors.success,
                    () => Navigator.pushNamed(context, '/pic')),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
