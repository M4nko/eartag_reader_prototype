import 'package:eartag_reader_prototype/screens/accrual/acrual_screen.dart';
import 'package:eartag_reader_prototype/screens/eartag_recognizer/eartag_recognizer_screen.dart';
import 'package:eartag_reader_prototype/screens/home_page/homepage_screen.dart';
import 'package:flutter/cupertino.dart';

import 'models/role.dart';

final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  '/': (BuildContext context) => MyHomePageScreen(
        title: 'Whatever',
      ),
  '/pic': (BuildContext context) => const EarTagRecognizerScreen(roles: [
        Role("Lamb", true),
        Role('Mother', false),
        Role('Father', false)
      ]),
  '/accrual': (BuildContext context) => AccrualScreen(),
};
